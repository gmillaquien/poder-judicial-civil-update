# Consultas hacia poder Judicial

Requerimientos Mínimos

-------------------------

# Si la página del poder judicial se cae, el script fallará y deberá reiniciar la ejecución.


- Instalar Python 2.7   [Descargar ](https://www.python.org/downloads/release/python-2717/),
el proyecto fue desarrollado en esta version de python, ya que existen limitaciones en las librerìas que se utilizan.

- Verificar que las variables de entorno se encuentre "C:\Python27;C:\Python27\Scripts;". Esto se puede verificar en mi pc.

![PATH ](/img/Path.png)

---------------------------------------

Se debe  descargar el driver correspondiente a nuestro navegador, por defecto este script utiliza el driver de google chrome.

Para revisar la versión, se debe ir a ayuda, acerca de y verificar su version del navegador.

![Acerca](/img/acercade.png)  


![Version Chrome](/img/version.png)  

De esta página descargamos el driver correspondiente y validámos que  sea correspondiente a la versión instalada en nuestro equipo.

 [Descargar  Driver Chrome ](https://chromedriver.chromium.org/downloads)


![Descarga Driver ](/img/descargaDriver.png)  

Copiamos el driver en la siguiente ubicación de nuestro PC, C:\Python27\Scripts.


![Copiar  Driver ](/img/driverexplorador.png)  


-------------------------------------



## Ejecución del Script


Descargamos el proyecto y lo descomprimimos.
Se escribe la palabra cmd en la barra de direcciones de windows para poder abrir una terminal cmd directamente y ejecutar los siguientes comandos.

![CMD](/img/cmdcarpeta.png)  

Ejecutar el comando   ``` .\env\Scripts\activate ``` .

![ENV](/img/env.png)  


Luego instalamos las dependencias.


Ejecutamos el comando y esperamos    ``` pip install -r requirements.txt    ``` . 


Para  que el script pueda guardar sin problemas en el archivo Excel, se debe reemplazar en la ruta  Proyecto/env/Lib/site-packages/ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.
-------------------------------------

![Version Chrome](/img/conexcel.png)  

-------------------------------------



En caso de que presente un error  realizar el siguiente copiado de archivo.

Para  que el script pueda guardar sin problemas en el archivo Excel, se debe reemplazar en la ruta  C:\Python27\Lib\site-packages\ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.


![Version Chrome](/img/contenido.png)  


Para iniciar el script ejecutamos lo siguiente  ``` python ejecutame.py ``` .

 

![Ejecutar](/img/ejecutar.png)  

Deberiamos poder visualizar como se  abre Chrome y realiza la búsqueda.

![Consultar](/img/consultasitioweb.png)  


Se verifica la información que se desea consultar, la cual debe estar en un excel con el nombre **Nombres.xls** dentro de la carpeta resultado.

![Contenido Resultado](/img/contenidoresultado.png)  

 
![Formato Excel](/img/excel.png)  




El  resultado se encontrara en la carpeta resultado.

![Resultado](/img/resultado.png)  