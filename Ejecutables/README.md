# Consultas hacia poder Judicial

Requerimientos Mínimos

-------------------------

- Instalar Python 2.7   [Descargar ](https://www.python.org/downloads/release/python-2717/)

- Verificar que las variables de entorno se encuentre "C:\Python27;C:\Python27\Scripts;", las  cuales se pueden  agregar cuando se instala Python.

---------------------------------------
Los siguientes aplicaciones se instalan a través de comandos en la consola  CMD.

Se verifica la instalación de Python.

![CMD ](/img/cmd.png)

Consola CMD 

![CMD 2 ](/img/cmd2.png)



---------------------------------------

- Instalar Robot Framework :

    ``` pip install robotframework ```
- Revisar si quedo el robot todo bien instalado:

     ``` robot --version ```
- Instalar wxPython:

     ``` pip install -U wxPython ```

- Instalar RIDE:

    ``` pip install robotframework-ride ```

---------------------------------------


Librerias necesarias para que el script funcione correctamente, también se instalan a tráves de la consola CMD.

-------------------------

- SeleniumLibrary

    ``` pip install robotframework-seleniumlibrary ```

- ExcelLibrary

     ``` pip install robotframework-excellibrary ```

- clipboard

    ``` pip install clipboard ```

-------------------------------------

Para terminar la instalación, debemos descargar el driver correspondiente a nuestro navegador, por defecto este script utiliza el driver de google chrome.


![Version Chrome](/img/version.png)  

De esta página descargamos el driver correspondiente y validámos que  sea correspondiente a la versión instalada en nuestro equipo.

 [Descargar  Driver Chrome ](https://chromedriver.chromium.org/downloads)


![Descarga Driver ](/img/descargaDriver.png)  

Para cambiar el Driver debemos descargar el driver del explorador que deseamos ocupar, agregarlo  en la ubicación C:\Python27\Scripts.


![Copiar  Driver ](/img/driverexplorador.png)  


Luego cambiamos el nombre del explorador  de chrome al explorador que se vaya a utilizar.

![Buscador de casos ](/img/buscadordecasos.png)  
-------------------------------------

Para  que el script pueda guardar sin problemas en el archivo Excel, se debe reemplazar en la ruta  C:\Python27\Lib\site-packages\ExcelLibrary, el contenido del rar que se adjunta  en los archivos del proyecto.


![Version Chrome](/img/contenido.png)  



-------------------------------------

## Ejecución del Script

Se inicia Ride y se carga el proyecto. Cuando se instala Ride, este preguntara si se quiere instalar un acceso directo, por lo cual deberia estar el icono en el escritorio.

![Ride ](/img/ride2.png)  


Se verifica la información que se desea consultar, la cual debe estar en un excel con el nombre **Nombres.xls** dentro de la carpeta resultado.

![Contenido Resultado](/img/contenidoresultado.png)  

 
![Formato Excel](/img/excel.png)  

![Ride](/img/ride.png)  

Cuando se ejecute el Script se  creará un archivo con el resultado de la consulta.


El  resultado.

![Resultado](/img/resultado.png)  